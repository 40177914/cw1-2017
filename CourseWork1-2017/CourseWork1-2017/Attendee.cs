﻿/*
 * Author : Fionn Mahnig 40177914
 * 
 * Description of class purpose: Class that will be able to save the values given in
 * the mainWindow form and will be able to send them back to the form or send the 
 * necessary values to invoice and certificate.
 * 
 * Date last modified: 21/10/2016
 */



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CourseWork1_2017
{
    class Attendee
    {
        private string firstName, lastName, instName, confName, regType, paperTitle;
       
        private int attendeeRef;
        private bool paid, presenter;

        #region ACCESSORS

        /// <summary>
        /// get and set first name + validation
        /// </summary>
        public string FirstName
        {
            get { return firstName; }
            set {
                if (value == "") //if there's no value in first name box
                {
                    MessageBox.Show("Please enter a value for first name."); //error message
                }
                else
                {
                    firstName = value; //else save value
                }
            }
        }
        /// <summary>
        /// get and set last name + validation
        /// </summary>
        public string LastName
        {
            get { return lastName; }
            set
            {
                if (value == "") //if there's no value in last name box
                    MessageBox.Show("Please enter a value for last name.");
                else
                    lastName = value;//else save value
            }
        }
        /// <summary>
        /// get  and set attendee ref + validation
        /// </summary>
        public int AttendeeRef
        {
            get { return attendeeRef; }
            set {
                if (value < 40000 || value > 60000) //if value is not between 40k and 60k
                {
                    MessageBox.Show("The Attendee Ref you entered must be between 40000 and 60000."); //don't save it
                  //  throw new ArgumentException("The value you entered must be between 40000 and 60000.");                   
                }
                else 
                  attendeeRef = value;  //save the value
            }
        }
        /// <summary>
        /// get and set inst name + validation
        /// </summary>
        public string InstName
        {
            get { return instName; }
            set { instName = value; }
        }
        /// <summary>
        /// get and set conference name + validation
        /// </summary>
        public string ConfName
        {
            get { return confName; }
            set {
                if (value == "") //if no value in conf name box
                    MessageBox.Show("Please enter a value for Conference Name"); //error message
                else
                    confName = value;  //else save value
            }
        }
        /// <summary>
        /// get and set registration type + validation
        /// </summary>
        public string RegType
        {
            get { return regType; }
            set { 
                if (value == "full" || value == "student" || value == "organiser") //if correct reg type
                    regType = value; //save value
                else
                    MessageBox.Show ("Registration type must be either: 'full', 'student' or 'organiser'"); //else error message
            }
        }

        //get the value of paid check box (true/false) + validation
        public bool Paid
        {
            get { return paid; }
            set { paid = value; }
        }

        /// <summary>
        /// get value of presenter check box (true/false) + validation
        /// </summary>
        public bool Presenter
        {
            get { return presenter; }
            set { presenter = value; }
        }

        /// <summary>
        /// get and set paper title + validation
        /// </summary>
        public string PaperTitle
        {
            get { return paperTitle; }
            set
            {                
                if (presenter == true && value == "")  //if presenter checked and value of paper title is null 
                {
                    MessageBox.Show("Please enter a paper title if you are a presenter"); //you must enter a paper title
                }
                else if (value != "" && presenter == false) //else if there's value in in paper title but presenter not checked 
                {
                    MessageBox.Show("You can not have a paper title if you are not a presenter."); //error message
                }
                else 
                    paperTitle = value; //else save value
            }
        }
        #endregion
        /// <summary>
        /// constructor
        /// </summary>
        public Attendee()
        {     
     
        }

        /// <summary>
        /// method that will get run when pressing the invoice button, this method will return the cost that will be displayed in the invoice, depending
        /// on the type of registration
        /// </summary>
        public double getCost()
        {
            double cost = 0;

            if (regType == "full")
                  cost = 500;
              else if (regType == "student")
                  cost = 300;
              else if (regType == "organiser")
                  cost = 0;
              else return cost;

              if (Presenter == true && regType != "organiser") //if presenter, 10% discount
                  return cost -= cost * 0.1;
              else return cost;

        }
    
    }
}
