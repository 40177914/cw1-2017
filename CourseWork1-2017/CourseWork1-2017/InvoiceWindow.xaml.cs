﻿/*
 * Author : Fionn Mahnig 40177914
 *  
 * Description of class purpose: Class that will display the correct information on the invoice window
 * invoice window will be called from mainwindow by pressing the invoice button when necessary values have been set
 * 
 * Date last modified: 21/10/2016
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CourseWork1_2017
{
    /// <summary>
    /// Interaction logic for InvoiceWindow.xaml
    /// </summary>
    public partial class InvoiceWindow : Window
    {
        private string firstName, lastName, instName, confName; //string values
        private double price; 

       /// <summary>
       /// Constuctor that gets all the data from main window to then show the data in the invoice window
       /// </summary>
       /// <param name="firstName">first name of attendee</param>
       /// <param name="lastName">last anme of attendee</param>
       /// <param name="instName">institution name</param>
       /// <param name="confName">conference name </param>
        /// <param name="price">price to be paid</param>
        public InvoiceWindow(string firstName, string lastName, string instName, string confName, double price)
        {
            //get all the values from attendee via main window constructor
            this.firstName = firstName;
            this.lastName = lastName;
            this.instName = instName;
            this.confName = confName;
            this.price = price;

            InitializeComponent();

            //show the values in text blocks
            txtNameBlock.Text = this.firstName + " " + this.lastName;
            txtInstBlock.Text = this.instName;
            txtConfBlock.Text = this.confName;
            txtPriceBlock.Text = "£"+this.price;
        }

    }
}
