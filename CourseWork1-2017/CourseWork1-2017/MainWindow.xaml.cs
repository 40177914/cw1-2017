﻿/*
 * Author : Fionn Mahnig 40177914
 * 
 * Description of class purpose: handles all the mainWindow GUI, code for all the buttons and text boxes 
 * 
 * Date last modified: 21/10/2016
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CourseWork1_2017
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Attendee attendee; //attendee class

        public MainWindow()
        {
            attendee = new Attendee(); //set up an attendee

            InitializeComponent();
        }
        /// <summary>
        /// clear the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            //clear all the fields
            txtAttendeeRef.Clear();
            txtConfName.Clear();
            txtFName.Clear();
            txtInstName.Clear();
            txtLName.Clear();
            chkPaid.IsChecked = false;
            txtPaperTitle.Clear();
            chkPresenter.IsChecked = false;
            txtRegType.Clear();

        }
        /// <summary>
        /// Code that will save the values input in the main form in variables in attendee
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
           //get first name, surname, conf name          
           attendee.FirstName = txtFName.Text;
           attendee.LastName = txtLName.Text;
           attendee.ConfName = txtConfName.Text; //get the name of the conference

           //convert attendee ref to int and get it checked by accessor
           int x = 0;
           Int32.TryParse(txtAttendeeRef.Text, out x);
           attendee.AttendeeRef = x;

           //get the name of the institution
           attendee.InstName = txtInstName.Text;
           //get the registration type
           attendee.RegType = txtRegType.Text.ToLower();


           //set the paid variable to true or false
           if (chkPaid.IsChecked == true) //if the checkbox is checked
               attendee.Paid = true; //paid is true           
           else
               attendee.Paid = false; //else it's false

           //set the presenter variable to true or false
           if (chkPresenter.IsChecked == true)
               attendee.Presenter = true;
           else
               attendee.Presenter = false;

           attendee.PaperTitle = txtPaperTitle.Text;
            
        }

        /// <summary>
        /// get all the info saved in attendee class and display in boxes when click on button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            txtFName.Text = attendee.FirstName;
            txtLName.Text = attendee.LastName;
            txtAttendeeRef.Text = attendee.AttendeeRef.ToString();
            txtInstName.Text = attendee.InstName;
            txtConfName.Text = attendee.ConfName;
            txtRegType.Text = attendee.RegType;
            chkPaid.IsChecked = attendee.Paid;
            chkPresenter.IsChecked = attendee.Presenter;
            txtPaperTitle.Text = attendee.PaperTitle;

        }
        /// <summary>
        /// when clicked, this button will open a new window showing an invoice using the getCost method from attendee.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {          
           //set all the values for the invoice window
           InvoiceWindow invWindow = new InvoiceWindow(attendee.FirstName, attendee.LastName, attendee.InstName, attendee.ConfName, attendee.getCost());
           //open invoice window
           invWindow.ShowDialog();
            
        }
        /// <summary>
        /// when clicked, open the certificate window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCertificate_Click(object sender, RoutedEventArgs e)
        {

           //set all the values to be shown in the new window opening for the certif
           CertificateWindow certWin = new CertificateWindow(attendee.FirstName, attendee.LastName, attendee.ConfName, attendee.PaperTitle, attendee.Presenter);
           //open the window
           certWin.ShowDialog();

        }
       
    }
}
