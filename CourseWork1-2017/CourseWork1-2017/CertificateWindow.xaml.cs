﻿/*
 * Author : Fionn Mahnig 40177914
 * 
 * Description of class purpose: Class that will display the correct information on the certificate window
 * certificate window will be called from mainwindow by pressing the certificate button when necessary values have been set
 * 
 * Date last modified: 21/10/2016
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CourseWork1_2017
{
    /// <summary>
    /// Interaction logic for CertificateWindow.xaml
    /// </summary>
    public partial class CertificateWindow : Window
    {
        private string firstName,lastName, confName, paperTitle;
        private bool presenter;
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="firstName">get first name value</param>
        /// <param name="lastName">get last name value</param>
        /// <param name="confName">get conference name value</param>
        /// <param name="paperTitle">get paper title</param>
        /// <param name="presenter">get presenter value (true/false)</param>
        public CertificateWindow(string firstName, string lastName, string confName, string paperTitle, bool presenter)
        {
            //get all the necessary values
            this.firstName = firstName;
            this.lastName = lastName;
            this.confName = confName;
            this.paperTitle = paperTitle;
            this.presenter = presenter;

            InitializeComponent();

            //if the person is not a presenter
            if (!presenter)
            {
                //hide the text about his paper
                lblIfPresenter.Visibility = System.Windows.Visibility.Hidden;
                txtPaperTitle.Visibility = System.Windows.Visibility.Hidden;
                //show just name and conference name
                txtName.Text = this.firstName + " " + this.lastName; 
                txtConfName.Text = this.confName;
            }
            else
            {
                //show the paper title as well as name and conf name
                lblIfPresenter.Visibility = System.Windows.Visibility.Visible;
                txtPaperTitle.Visibility = System.Windows.Visibility.Visible;
                txtName.Text = this.firstName + " " + this.lastName; //show the first name and surname
                txtConfName.Text = this.confName; //show conf name
                txtPaperTitle.Text = this.paperTitle; //show paper title
            }
        }
    }
}
